#include <iostream>
using namespace std;
#include <conio.h>
#include <stdlib.h>
#include <windows.h>
#include <cstdlib>
#include <ctime>
#include <MMsystem.h>

//prototypes
int checkwin(char []);
void board(char [], char ,char,int, string, string, string);
int computermove (char[], char, char);

int main()
{

     //Coloring the background:
    system("color 3F");
    std::cout<<""<<std::endl;

    //variables
    char x, y;
    int player = 1,i,choice,turnFirst, mode;
    int com, firstturn=1;
    char square[10] = {'o','1','2','3','4','5','6','7','8','9'};
    char mark=0;
    string name1,name2,player1,player2;
    int length1,length2;
    char modechange, rematch;


    //title
    cout<<"\t\t\t\t\t\t___________.__               ___________                     ___________  "<<endl;
    cout<<"\t\t\t\t\t\t\\__    ___/|__| ____         \\__    ___/____    ____         \\__    ___/___   ____  "<<endl;
    cout<<"\t\t\t\t\t\t  |    |   |  |/ ___\\   ______ |    |  \\__  \\ _/ ___\\   ______ |    | /  _ \\_/ __ \\ "<<endl;
    cout<<"\t\t\t\t\t\t  |    |   |  \\  \\___  /_____/ |    |   / __ \\\\  \\___  /_____/ |    |(  <_> )  ___/ "<<endl;
    cout<<"\t\t\t\t\t\t  |____|   |__|\\___  >         |____|  (____  /\\___  >         |____| \\____/ \\___  >"<<endl;
    cout<<"\t\t\t\t\t\t                   \\/                       \\/     \\/                            \\/ "<<endl;

    //Changing the Colors in a row:
    system ( "color 3A" );
    Sleep ( 300 );

    system ( "color 3B" );
    Sleep ( 300 );

    system ( "color 3C" );
    Sleep ( 300 );

    system ( "color 3D" );
    Sleep ( 300 );

    system ( "color 3E" );
    Sleep ( 300 );

    system ( "color 3F" );
    Sleep ( 300 );

    system ( "color 3A" );
    Sleep ( 300 );

    system ( "color 3B" );
    Sleep ( 300 );

    system ( "color 3C" );
    Sleep ( 300 );

    system ( "color 3D" );
    Sleep ( 300 );

    system ( "color 3E" );
    Sleep ( 300 );

    system ( "color 3F" );
    Sleep ( 300 );


    system("color 3F");
    std::cout<<""<<std::endl;


    //intro
    cout<<"\n\n\n\n\t\t\t\t\t\t\tWelcome to the game! Press any button to continue........"<<endl;
    getch();

    cout<<"\n\t\t\t\t\t\t\t\t\t\tChoose mode:"<<endl;
    cout<<"\n\t\t\t\t\t\t\t<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>"<<endl;
    cout<<"\n\t\t\t\t\t\t\t1. Player 1 vs. Player 2\t2. Player 1 vs. Computer "<<endl;
    cout<<"\n\t\t\t\t\t\t\t<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>"<<endl;
    cout<<"\n\t\t\t\t\t\t\t\t\t\tMode: ";
    cin>>mode;

    cout<<"\n\n\t\t\t\t\t\t\t<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>"<<endl;

    system ("cls");

    //modes
    if (mode==1)
    {
    cout<<"\n\t\t\t\t\t\t\t\t\tMode: Player 1 vs. Player 2 "<<endl;


        //choosing which is player 1
    cout<< "\n\n\t\t\t\t\t\t\t\t\tEnter player names: ";
    cin>>name1;
    length1=name1.length();
    cin>>name2;
    length2=name2.length();

	if (length1>length2)
	{
    player1=name1;
	player2=name2;
	}
	else if (length2>length1)
	{
	player1=name2;
	player2=name1;
	}
	else
	{
	player1=name2;
	player2=name1;
	}
    cout<< "\n\n\t\t\t\t\t\t\t\t\tPlayer 1: "<<player1<<endl;
    cout<< "\t\t\t\t\t\t\t\t\tPlayer 2: "<<player2<<endl;
    cout<< "\t\t\t\t\t\t\t\t\tPlayer 1 will go first."<<endl;

    //choosing symbol
    cout<<"\n\t\t\t\t\t\t\t\t\tChoose symbol for game: "<<endl;
    cout<<"\t\t\t\t\t\t\t\t\tPlayer 1: ";
    cin>>x;
    cout<<"\t\t\t\t\t\t\t\t\tPlayer 2: ";
    cin>>y;

    cout<<"\n\n\t\t\t\t\t\t\t\t\tBegin!!!"<<endl;

    }

    else
    {
    cout<<"\n\t\t\t\t\t\t\t\t\tMode: Player 1 vs. Computer "<<endl;

    //choosing which is player 1
    cout<<"\n\n\t\t\t\t\t\t\t\t\tEnter player name: ";
    cin>>name1;
    length1=name1.length();
    name2= "Computer";
    length2=name2.length();

	if (length1>length2)
	{
    player1=name1;
	player2=name2;
	}
	else if (length2>length1)
	{
	player1=name2;
	player2=name1;
	}
	else
	{
	player1=name2;
	player2=name1;
	}
    cout<< "\n\n\t\t\t\t\t\t\t\t\tPlayer 1: "<<player1<<endl;
    cout<< "\t\t\t\t\t\t\t\t\tPlayer 2: "<<player2<<endl;
    cout<< "\t\t\t\t\t\t\t\t\tPlayer 1 will go first"<<endl;

    //choosing symbol\t\t\t\t
    cout<<"\n\t\t\t\t\t\t\t\t\tChoose symbol for game: "<<endl;
    cout<<"\t\t\t\t\t\t\t\t\tPlayer: ";
    cin>>x;
    cout<<"\t\t\t\t\t\t\t\t\tComputer symbol: # "<<endl;
    y='#';

    cout<<"\n\n\t\t\t\t\t\t\t\t\tBegin!!!"<<endl;
    }
    cout<<"\n\t\t\t\t\t\t\t<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>:<:>"<<endl;
    getch();



    do{ // loop for rematch

   do{  //main loop for entering symbol in chart


	    if (mode==1)
        {
        board(square,x,y,mode, player1, player2, name1);

		player=(player%2)?1:2;

		cout << "\n\t\t\t\t\t\t\t\t\tPlayer " << player << ", enter a number:  ";
		cin >> choice;
        mark=(player == 1) ? x : y;
        PlaySound(TEXT("checkmark.wav"),NULL,SND_SYNC);

          //changing number in the chart

		if (choice == 1 && square[1] == '1'){

			square[1] = mark;
			}
		else if (choice == 2 && square[2] == '2'){

			square[2] = mark;
		}
		else if (choice == 3 && square[3] == '3'){

			square[3] = mark;
		}
		else if (choice == 4 && square[4] == '4'){

			square[4] = mark;
		}
		else if (choice == 5 && square[5] == '5'){

			square[5] = mark;
		}
		else if (choice == 6 && square[6] == '6'){

			square[6] = mark;
		}
		else if (choice == 7 && square[7] == '7'){

			square[7] = mark;
		}
		else if (choice == 8 && square[8] == '8'){

			square[8] = mark;
		}
		else if (choice == 9 && square[9] == '9'){

			square[9] = mark;
		}
		else
		{
			cout<<"\n\t\t\t\t\t\t\t\t\tInvalid move ";

			player--;
			cin.ignore();
			cin.get();
		}
        i=checkwin(square);
        player++;


	}

    else     //mode 2
    {
        board(square,x,y,mode,player1,player2, name1);

		player=(player%2)?1:2;

		if (player1==name1 && player==1 || player2==name1 && player==2){

		cout << "\n\t\t\t\t\t\t\t\t\tPlayer " << player << ", enter a number:  ";
		cin >> choice;
		PlaySound(TEXT("checkmark.wav"),NULL,SND_SYNC);

		mark=x;
		}

		else
        {
         choice= computermove(square , x,  y);
         //calling function to generate computer's move
        cout << "\n\t\t\t\t\t\t\t\t\tPlayer " << player << "--Computer:  "<<choice;
		mark=y;
		getch();
		PlaySound(TEXT("checkmark.wav"),NULL,SND_SYNC);
        }

         //changing number in chart

		if (choice == 1 && square[1] == '1'){

			square[1] = mark;
		}
		else if (choice == 2 && square[2] == '2'){

			square[2] = mark;
		}
		else if (choice == 3 && square[3] == '3'){

			square[3] = mark;
		}
		else if (choice == 4 && square[4] == '4'){

			square[4] = mark;
		}
		else if (choice == 5 && square[5] == '5'){

			square[5] = mark;
		}
		else if (choice == 6 && square[6] == '6'){

			square[6] = mark;
		}
		else if (choice == 7 && square[7] == '7'){

			square[7] = mark;
		}
		else if (choice == 8 && square[8] == '8'){

			square[8] = mark;
		}
		else if (choice == 9 && square[9] == '9'){

			square[9] = mark;
		}
		else{

			cout<<"\n\t\t\t\t\t\t\t\t\tInvalid move ";

			player--;
			cin.ignore();
			cin.get();
    }
     //checking if a player has won
        i=checkwin(square);
        player++;

	}
	}
	while(i==1);
    board(square,x,y,mode,player1,player2, name1);



	//determine winner
     if (i==-1)
     {
         cout<<"\n\t\t\t\t\t\t\t\t\tPlayer "<<--player<<" has won!!!"<<endl;
         PlaySound(TEXT("win.wav"),NULL,SND_SYNC);

     }

     else
     {
         cout<<"\n\t\t\t\t\t\t\t\t\tGame is a tie!!"<<endl;
     }

	cin.ignore();
	cin.get();

	//rematch
    //rematch
    cout<<"\t\t\t\t\t\t\t\t\tWould you like a rematch? (y/n): ";
    cin>>rematch;
    system ("cls");

    }

    while (rematch== 'y' || rematch== 'Y');

    return 0;
}

int checkwin(char square[])  //function for checking if a player has won and stopping loop
{
	if (square[1] == square[2] && square[2] == square[3])

		return -1;
	else if (square[4] == square[5] && square[5] == square[6])

		return -1;
	else if (square[7] == square[8] && square[8] == square[9])

		return -1;
	else if (square[1] == square[4] && square[4] == square[7])

		return -1;
	else if (square[2] == square[5] && square[5] == square[8])

		return -1;
	else if (square[3] == square[6] && square[6] == square[9])

		return -1;
	else if (square[1] == square[5] && square[5] == square[9])

		return -1;
	else if (square[3] == square[5] && square[5] == square[7])

		return -1;
	else if (square[1] != '1' && square[2] != '2' && square[3] != '3' && square[4] != '4' && square[5] != '5' && square[6] != '6' && square[7] != '7' && square[8] != '8' && square[9] != '9')

		return 0;
	else
		return 1;
}

void board(char square[], char x, char y,int mode,string player1, string player2, string name1)
{
    //function to draw board
    system("cls");

    if (mode==1)
    {

	cout<<""<<endl;
    cout<<""<<endl;
    cout<<""<<endl;

    cout<<"\n\t\t\t\t\t\t\t\t\tPlayer 1-"<<player1<< " ("<<x<<") -- Player 2-"<<player2<< " ("<<y<<")\n\n"<<endl;

    cout<<"\t\t\t\t\t\t\t\t\t\t|\t\t|"<<endl;
    cout<<"\t\t\t\t\t\t\t\t\t"<<square[1]<<"\t|\t"<<square[2]<<"\t|    "<<square[3]<<endl;
    cout<<"\t\t\t\t\t\t\t\t\t\t|\t\t|"<<endl;

    cout<<"\t\t\t\t\t\t\t\t\t----------------------------------"<<endl;

   cout<<"\t\t\t\t\t\t\t\t\t\t|\t\t|"<<endl;
    cout<<"\t\t\t\t\t\t\t\t\t"<<square[4]<<"\t|\t"<<square[5]<<"\t|    "<<square[6]<<endl;
    cout<<"\t\t\t\t\t\t\t\t\t\t|\t\t|"<<endl;

    cout<<"\t\t\t\t\t\t\t\t\t----------------------------------"<<endl;

   cout<<"\t\t\t\t\t\t\t\t\t\t|\t\t|"<<endl;
    cout<<"\t\t\t\t\t\t\t\t\t"<<square[7]<<"\t|\t"<<square[8]<<"\t|    "<<square[9]<<endl;
    cout<<"\t\t\t\t\t\t\t\t\t\t|\t\t|"<<endl;
    }

    else
    {
    cout<<""<<endl;
    cout<<""<<endl;
    cout<<""<<endl;

	if (player1==name1){
    cout<<"\n\t\t\t\t\t\t\t\t\tPlayer 1-"<<player1<< " ("<<x<<") -- Player 2-"<<player2<< "("<<y<< ")\n\n"<<endl;
    }

    else if (player2==name1){
    cout<<"\n\t\t\t\t\t\t\t\t\tPlayer 1-"<<player1<< " ("<<y<<") -- Player 2-"<<player2<< "("<<x<< " )\n\n"<<endl;
    }

    cout<<"\t\t\t\t\t\t\t\t\t\t|\t\t|"<<endl;
    cout<<"\t\t\t\t\t\t\t\t\t"<<square[1]<<"\t|\t"<<square[2]<<"\t|    "<<square[3]<<endl;
    cout<<"\t\t\t\t\t\t\t\t\t\t|\t\t|"<<endl;

    cout<<"\t\t\t\t\t\t\t\t\t----------------------------------"<<endl;

    cout<<"\t\t\t\t\t\t\t\t\t\t|\t\t|"<<endl;
    cout<<"\t\t\t\t\t\t\t\t\t"<<square[4]<<"\t|\t"<<square[5]<<"\t|    "<<square[6]<<endl;
    cout<<"\t\t\t\t\t\t\t\t\t\t|\t\t|"<<endl;

    cout<<"\t\t\t\t\t\t\t\t\t----------------------------------"<<endl;

    cout<<"\t\t\t\t\t\t\t\t\t\t|\t\t|"<<endl;
    cout<<"\t\t\t\t\t\t\t\t\t"<<square[7]<<"\t|\t"<<square[8]<<"\t|    "<<square[9]<<endl;
    cout<<"\t\t\t\t\t\t\t\t\t\t|\t\t|"<<endl;
    }

}
int computermove (char square [], char x, char y)  //function o generate computer's move
{
    int r;


   if(square[1] == '1' && (square[2] == square[3] || square [4] == square[7] || square[5] == square[9]))
    {
    return 1;
    }
    else if(square[2] == '2' && (square[1] == square[3] || square [5] == square[8]))
    {
    return 2;
    }
    else if(square[3] == '3' && (square[1] == square[2] || square [6] == square[9] || square [5] == square[7]))
    {
    return 3;
    }
    else if(square[4] == '4' && (square[5] == square[6] || square [1] == square[7]))
    {
    return 4;
    }
    else if(square[5] == '5' && (square[4] == square[6] || square [2] == square[8] || square [1] == square[9] || square[7] == square[3]))
    {
    return 5;
    }
    else if(square[6] == '6' && (square[4] == square[5] || square [3] == square[9]))
    {
    return 6;
    }
    else if(square[7] == '7' && (square[8] == square[9] || square [1] == square[4] || square [5] == square[3]))
    {
    return 7;
    }
    else if(square[8] == '8' && (square[7] == square[9] || square [2] == square[5]))
    {
    return 8;
    }
    else if(square[9] == '9' && (square[7] == square[8] || square [3] == square[6] || square [1] == square[5]))
    {
    return 9;
    }
    else
    {

    //COMPUTER MOVES RANDOMLY

    srand(time(0));
    for(int i=1; i<10; i++)
    {
    if(square[i] != x && square[i] != y)
    {
    r=(1+(rand()%9));
    }
    }

    return r;

    }
}
